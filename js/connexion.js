$(function() {
    $("#submitConnex").click(function(e){
        e.preventDefault();
        var pseudo = $("#pseudo").val();
        var pwd = $("#passe").val();
        
        if(pseudo == "" || pwd == ""){
            $("#erreur").css('display', 'block');
            $("#passe").attr('style','border:red 1px solid');
            $("#pseudo").attr('style','border:red 1px solid');
        }
        else{ 
            var user = localStorage.getItem(pseudo);
            if(user == null){
                $("#erreur").css('display', 'block');
                $("#erreur").text("Utilisateur non connu");
            }
            else{
                user = JSON.parse(user);
                if(pwd == user.pwd){
                    localStorage.setItem("user",user.pseudo);
                    $("#navNonCo").css('display', 'none');
                    $("#navCo").css('display', 'block');
                    document.location.href="index.html";
                }
                else{
                    $("#erreur").css('display', 'block');
                    $("#erreur").text("Mot de passe incorrect");
                }
            }
        }
    })
    
    $("input").click(function(){
        $(this).attr('style','border:solid 1px rgb(206, 212, 218);');
        $("#erreur").fadeOut();
    });
});