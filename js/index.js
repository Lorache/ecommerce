$(function() {
	if(localStorage.getItem('user')){
		$("#navNonCo").css('display', 'none');
		$("#navCo").css('display', 'block');
	}
	else{
		$("#navCo").css('display', 'none');
		$("#navNonCo").css('display', 'block');
	}
});