$(function() {
    $("#submitInscri").click(function(e){
        e.preventDefault();

        var email = $("#email").val();
        var pseudo = $("#pseudo").val();
        var pwd = $("#passe").val();

        if(email == "" || pseudo == "" || pwd == ""){
            $("#erreur").css('display', 'block');
            $("#email").attr('style','border:red 1px solid');
            $("#pseudo").attr('style','border:red 1px solid');
            $("#passe").attr('style','border:red 1px solid');
        }
        else if(!email.match(/[a-z0-9_\-\.]+@[a-z0-9_\-\.]+\.[a-z]+/i)){
            $("#erreur").css('display', 'block');
            $("#erreur").text("format adresse mail non valide");
        }
        else{
            var user = JSON.stringify({
                "email" : email,
                "pseudo": pseudo,
                "pwd":pwd
            })
            localStorage.setItem(pseudo,user);
            document.location.href="index.html";
        }
    });

    $("input").click(function(){
        $(this).attr('style','border:solid 1px rgb(206, 212, 218);');
        $("#erreur").fadeOut();
    });
});