$(function () {

    jQuery.when(
        jQuery.getJSON('./json/data.json')
    ).done(function (json) {

        for (var i = 0; i < json.Moto.length; i++) {

            var div = document.createElement('div');
            div.classList.add('card');
            div.setAttribute('style', 'width: 50rem;')
            div.setAttribute('id', 'modele_moto');

            var img = document.createElement('img');
            img.setAttribute('src', json.Moto[i].Image);
            img.classList.add('card-img-top');

            var div2 = document.createElement('div');
            div2.classList.add('card-body');
            div2.setAttribute("id", "div2");

            var h5 = document.createElement('h5');
            h5.classList.add('card-title');
            h5.setAttribute('style', 'text-align: center')
            h5.innerHTML = json.Moto[i].Modele;

            var p = document.createElement('p');
            p.classList.add('card-text');
            p.innerHTML = json.Moto[i].Description;

            var secondH5 = document.createElement('h5');
            secondH5.innerHTML = json.Moto[i].Prix;

            var butt = document.createElement('button');
            butt.setAttribute('href', 'monlien');
            butt.setAttribute('id', json.Moto[i].Id)
            butt.classList.add('btn', 'btn-primary');
            butt.innerHTML = "Ajouter au panier";

            div2.appendChild(p);
            div2.appendChild(secondH5);
            div2.appendChild(butt);
            div.appendChild(h5);
            div.appendChild(img);

            div.appendChild(div2);

            $("#modeles").append(div);
        }
    });
});